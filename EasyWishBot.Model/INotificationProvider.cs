﻿using EasyWishBot.Model.Commands;

namespace EasyWishBot.Model
{
    public interface INotificationProvider
    {
        void Add(NotificationKey key);
        void Remove(NotificationKey key);
    }
}