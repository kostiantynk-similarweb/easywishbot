﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using EasyWishBot.Model.Services;
using Telegram.Bot;

namespace EasyWishBot.Model
{
    public class BirthDayCheker : IBirthDayCheker
    {
        private readonly DataContext _context;
        private readonly TelegramBotClient _client;
        private readonly IImageSender _imageSender;

        private Timer _timer;
        public BirthDayCheker(DataContext context, TelegramBotClient client, IImageSender imageSender)
        {
            _context = context;
            _client = client;
            _imageSender = imageSender;
        }

        public void Init()
        {
              _timer = new Timer(Callback, null, 0, 1000 * 60 * 60 * 24);
            
        }

        private void Callback(object state)
        {

         var usersWithUpcomingBirthDays=  _context.Users.Where(s => GetUpcomingBirthDay(s));


            foreach (var user in usersWithUpcomingBirthDays)
            {
                _client.SendTextMessageAsync(user.ChatId, $"@{user.UserName}'s birthday is comming in 7 days");
            }

            var birthdayToday = _context.Users.Where(s => GetTodayBirthDay(s));

            foreach (var user in birthdayToday)
            {                
                _client.SendTextMessageAsync(user.ChatId, $" Happy birthday @{user.UserName}");
                _imageSender.SendImagesAsync(_client, user.ChatId, "cake", 5, true);
            }


        }

        private bool GetUpcomingBirthDay(User user)
        {
            var currentYearBirthDay= new DateTime(DateTime.Now.Year,user.BirthDay.Month,user.BirthDay.Day);

            var diff=currentYearBirthDay - DateTime.Now;
            return diff.Days >= 7 ;
        }

        private bool GetTodayBirthDay(User user)
        {
            var currentYearBirthDay = new DateTime(DateTime.Now.Year, user.BirthDay.Month, user.BirthDay.Day);

            var diff = currentYearBirthDay - DateTime.Now;
            return diff.Days ==0;
        }

    }
}
