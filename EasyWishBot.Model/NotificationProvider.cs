﻿using System;
using System.Collections.Generic;
using EasyWishBot.Model.Commands;

namespace EasyWishBot.Model
{
    public class NotificationProvider : INotificationProvider
    {
        private readonly string _token;

        private IDictionary<string, NotificationTimer> _notificationCollection;

        public NotificationProvider(string token)
        {
            _token = token;

            _notificationCollection = new Dictionary<string, NotificationTimer>();
        }


        public void Add(NotificationKey key)
        {
            NotificationTimer timer;
            if (_notificationCollection.TryGetValue(key.ToString(), out timer))
            {

            }
            else
            {
                var nTimer = new NotificationTimer(key);

                nTimer.Elapsed += NotifyHandler;
                _notificationCollection.Add(key.ToString(), nTimer);
            }
        }

        public void Remove(NotificationKey key)
        {
            NotificationTimer timer; 


            if (_notificationCollection.TryGetValue(key.ToString(), out timer))
            {
                timer.Stop();
                timer.Elapsed -= NotifyHandler;
                _notificationCollection.Remove(key.ToString());
            }

            var bot = new Telegram.Bot.TelegramBotClient(_token);

            bot.SendTextMessageAsync(key.ChatId, "Birthday notification turned off");

        }

        public void NotifyHandler(long chatId, string userName, DateTime birthDay, bool passed)
        {
            var bot = new Telegram.Bot.TelegramBotClient(_token);

            if (passed)
            {
                bot.SendTextMessageAsync(chatId, $"Birth day of {userName} already passed on, {birthDay:MMMM}  {birthDay.Day}");
            }
            else
            {
                bot.SendTextMessageAsync(chatId, $"Birth day of {userName} is comming on, {birthDay:MMMM}  {birthDay.Day}");
            }            
        }



    }
}
