﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EasyWishBot.Model
{
    public class User
    {
        [Key]
        public Guid Id { get; set; }

        public long ChatId { get; set; }

        public long UserId { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DateTime BirthDay { get; set; }
    }
}
