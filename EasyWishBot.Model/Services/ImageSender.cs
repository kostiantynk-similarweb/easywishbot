﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace EasyWishBot.Model.Services
{
    public interface IImageSender
    {
        Task<bool> SendImagesAsync(TelegramBotClient client, ChatId chatId, string query, int count, bool imageSmall);
    }

    public class ImageSender : IImageSender
    {
        private readonly IImageGetter _imageGetter;
        
        public ImageSender(IImageGetter getter)
        {
            _imageGetter = getter;
        }

        public async Task<bool> SendImagesAsync(TelegramBotClient bot, ChatId chatId, string query, int count, bool imageSmall)
        {
            if (string.IsNullOrEmpty(query) || bot == null)
            {
                throw new ArgumentException("image sender arguments exception");
            }

            string q = WebUtility.UrlEncode(string.Join(" ", query));
            var pictures = await _imageGetter.GetImages(q, count, imageSmall).ConfigureAwait(false);

            if (pictures == null || !pictures.Any())
            {
                return false;
            }

            Random r = new Random();
            int maxCount = count >= pictures.Count() ? pictures.Count() : count;
            int index = r.Next(0, maxCount);
            var pic = pictures[index];
            
            using (HttpClient client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync(pic).ConfigureAwait(false))
            using (HttpContent content = response.Content)
            {
                var imageStream = await content.ReadAsStreamAsync().ConfigureAwait(false);
                await bot.SendPhotoAsync(chatId, new FileToSend("image" + r.Next(10000), imageStream)).ConfigureAwait(false);
            }
            
            return true;
        }
    }
}
