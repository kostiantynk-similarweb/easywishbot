﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using EasyWishBot.Model.Models;
using Newtonsoft.Json;
using HtmlAgilityPack;

namespace EasyWishBot.Model.Services
{
    public interface IImageGetter
    {
        Task<IList<string>> GetImages(string query, int count, bool imageSmall);
    }

    public class ImageGetter : IImageGetter
    {
        //https://pixabay.com/api/?key=6129997-7b0d3c8b898350c7e22d86514&q=world 

        //https://code.msdn.microsoft.com/How-to-parse-html-in-NET-2660026c
        //http://images.yandex.ru/yandsearch?text=world

        private const string DOMAIN = "https://pixabay.com/api/";
        private const string KEY = "6129997-7b0d3c8b898350c7e22d86514";

        public async Task<IList<string>> GetImages(string query, int count, bool imageSmall)
        {
            if (string.IsNullOrEmpty(query))
            {
                throw new ArgumentException("query");
            }

            string url = $"{DOMAIN}?key={KEY}&q={WebUtility.UrlEncode(query)}";

            using (HttpClient client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync(url).ConfigureAwait(false))
            using (HttpContent content = response.Content)
            {
                string result = await content.ReadAsStringAsync().ConfigureAwait(false);

                var jsonObj = JsonConvert.DeserializeObject<ImageResponse>(result);
                return jsonObj.hits.Take(count).Select(x => imageSmall ? x.previewURL : x.webformatURL).ToList();
            }
        }
    }

    public class YandexImageGetter : IImageGetter
    {
        private const string DOMAIN = "https://www.google.com.ua/search?";
        //q=sun&source=lnms&tbm=isch

        public async Task<IList<string>> GetImages(string query, int count, bool imageSmall)
        {
            if (string.IsNullOrEmpty(query))
            {
                throw new ArgumentException("query");
            }

            string url = $"{DOMAIN}q={WebUtility.UrlEncode(query)}&source=lnms&tbm=isch";

            using (HttpClient client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync(url).ConfigureAwait(false))
            using (HttpContent content = response.Content)
            {
                var stream = await content.ReadAsStreamAsync().ConfigureAwait(false);

                HtmlDocument doc = new HtmlDocument();
                doc.Load(stream);

                HtmlNodeCollection imgs = doc.DocumentNode.SelectNodes("//img");

                var images = imgs
                    .Select(x => x.Attributes.AttributesWithName("src").FirstOrDefault()?.Value)
                    .Where(x=> !string.IsNullOrEmpty(x) && x.StartsWith("http"))
                    .Take(count)
                    .ToList();

                return images;
            }
        }
    }
}
