﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyWishBot.Model.Models
{
    public class ImageResponse
    {
        public int totalHits;
        public ImageHit[] hits;
        public int total;
    }

    public class ImageHit
    {
        public int previewHeight;
        public int likes;
        public int favorites;
        public string tags;
        public string pageURL;
        public string previewURL;
        public string webformatURL;
        public int imageWidth;
        public int imageHeight;
    }
}
