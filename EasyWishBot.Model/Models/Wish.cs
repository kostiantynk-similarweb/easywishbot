﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EasyWishBot.Model.Models
{
    public class Wish
    {
        [Key]
        public Guid Id { get; set; }

        public string UserName { get; set; }
        public string Message { get; set; }
        public string Author { get; set; }
    }
}
