﻿using System;
using System.Collections.Generic;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace EasyWishBot.Model.Extensions
{
    public static class MessageExtensions
    {
        public static Tuple<MessageEntity, string> ExtractFirstEntity(this Message message, MessageEntityType messageEntityType)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));
            if (message.Entities == null)
                throw new ArgumentException(nameof(message.Entities));
            if (message.EntityValues == null)
                throw new ArgumentException(nameof(message.EntityValues));
            for (var i = 0; i < message.Entities.Count; i++)
            {
                var entity = message.Entities[i];
                var value = message.EntityValues[i];
                if (entity.Type == messageEntityType)
                {
                    return new Tuple<MessageEntity, string>(entity, value);
                }
            }
            return null;
        }

        public static IEnumerable<Tuple<MessageEntity, string>> ExtractEntities(this Message message, MessageEntityType messageEntityType)
        {
            if (message == null)
                throw new ArgumentNullException(nameof(message));
            if (message.Entities == null)
                throw new ArgumentException(nameof(message.Entities));
            if (message.EntityValues == null)
                throw new ArgumentException(nameof(message.EntityValues));
            for (var i = 0; i < message.Entities.Count; i++)
            {
                var entity = message.Entities[i];
                var value = message.EntityValues[i];
                if (entity.Type == messageEntityType)
                {
                    yield return new Tuple<MessageEntity, string>(entity, value);
                }
            }
        }
    }
}
