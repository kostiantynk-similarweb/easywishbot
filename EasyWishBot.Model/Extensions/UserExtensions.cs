﻿using System;
using System.Linq;

namespace EasyWishBot.Model.Extensions
{
    public static class UserExtensions
    {
        public static string GetFullName(this User user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            if (!String.IsNullOrEmpty(user.FirstName) || !String.IsNullOrEmpty(user.LastName))
            {
                return String.Join(" ", new [] {user.FirstName, user.LastName}.Where(w => w != null));
            }
            return "@" + user.UserName;
        }
    }
}
