﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace EasyWishBot.Model
{
    public static class ParameterListExtensions
    {
        public static IList<string> ExtractUserNames(this IList<string> parameters)
        {
            if (parameters == null)
                throw new ArgumentNullException(nameof(parameters));
            return parameters
                .Where(x => x.StartsWith("@"))
                .Select(x => x.Substring(1))
                .ToList();
        }

        public static string ExtractFirstUserName(this IList<string> parameters)
        {
            if (parameters == null)
                throw new ArgumentNullException(nameof(parameters));
            return parameters
                .Where(x => x.StartsWith("@"))
                .Select(x => x.Substring(1))
                .FirstOrDefault();
        }

        public static IList<DateTime> ExtractDateTimes(this IList<string> parameters)
        {
            if (parameters == null)
                throw new ArgumentNullException(nameof(parameters));
            return parameters
                .Select(x => DateTime.TryParseExact(x, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime date) ? date : DateTime.MinValue)
                .Where(x => x != DateTime.MinValue)
                .ToList();
        }

        public static DateTime? ExtractFirstDateTime(this IList<string> parameters)
        {
            if (parameters == null)
                throw new ArgumentNullException(nameof(parameters));
            return parameters
                .Select(x => DateTime.TryParseExact(x, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime date) ? date : DateTime.MinValue)
                .FirstOrDefault(x => x != DateTime.MinValue);
        }

    }
}
