﻿
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Threading;
using EasyWishBot.Model.Commands;
using Telegram.Bot.Types;

namespace EasyWishBot.Model
{
    public class NotificationTimer
    {
        private readonly long _chatId;
        private readonly int _interval;
        private readonly string _userName;
        private readonly DateTime _birthDay;
        private bool _passed;

        public event NotifyHandler Elapsed;

        private readonly Timer _timer;


        public NotificationTimer(NotificationKey key)
        {
            _chatId = key.ChatId;
            _interval = key.Interval;
            _userName = key.User;
            _birthDay = key.BirthDay;
            _passed = key.Interval == 1;

            _timer = new Timer(Callback, null, _interval, Timeout.Infinite);
            
        }

        private void Callback(object state)
        {
            Elapsed?.Invoke(_chatId, _userName,_birthDay, _passed);
        }

        public void Stop()
        {
            _timer.Dispose();
        }
    }

    public delegate void NotifyHandler (long chatId, string userName, DateTime birthDay, bool passed);
}