﻿using System;
using System.Collections.Generic;
using System.Text;
using EasyWishBot.Model.Models;

namespace EasyWishBot.Model.Repository
{
    public interface IWishRepository : IRepository<Wish, string>
    {
    }
}
