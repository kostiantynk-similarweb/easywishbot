﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EasyWishBot.Model.Models;

namespace EasyWishBot.Model.Repository
{
    public class WishRepository : IWishRepository
    {
        private readonly DataContext _provider;

        public WishRepository(DataContext provider)
        {
            _provider = provider;
        }

        public Wish GetById(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return null;
            }

            return _provider.Wishes.FirstOrDefault(x => x.Id.ToString().Equals(id, StringComparison.OrdinalIgnoreCase));
        }

        public IList<Wish> GetAll()
        {
            return _provider.Wishes.ToList();
        }

        public void Create(Wish entity)
        {
            _provider.Wishes.Add(entity);
            _provider.SaveChanges();
        }

        public void Update(Wish entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(string id)
        {
            var wish = GetById(id);

            if (wish == null)
            {
                return;
            }

            _provider.Wishes.Remove(wish);
            _provider.SaveChanges();
        }
    }
}
