﻿using System.Collections.Generic;

namespace EasyWishBot.Model.Repository
{
    public interface IRepository<TEntity, in TKey> where TEntity : class
    {
        TEntity GetById(TKey id);
        IList<TEntity> GetAll();
        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(TKey id);
    }
}
