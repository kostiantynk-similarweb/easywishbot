﻿using System;
using System.Linq;

namespace EasyWishBot.Model
{
    public static class DataContextUserExtensions
    {
        public static User GetUser(this DataContext context, long chatId, string username)
        {
            return context.Users.SingleOrDefault(x => x.ChatId == chatId && x.UserName == username);
        }

        public static bool IsUserExist(this DataContext context, long chatId, string username)
        {
            return context.Users.Any(x => x.ChatId == chatId && x.UserName == username);
        }

        public static void SetUser(this DataContext context, User user)
        {
            var entity = context.GetUser(user.ChatId, user.UserName);
            if (entity == null)
            {
                user.Id = Guid.NewGuid();
                context.Users.Add(user);
            }
            else
            {
                entity.BirthDay = user.BirthDay;
            }
            context.SaveChanges();
        }
    }
}
