﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace EasyWishBot.Model.Commands
{
    public interface ICommand
    {
        Task ExecuteAsync(TelegramBotClient bot, Message original, IList<string> parameteres);
    }
}
