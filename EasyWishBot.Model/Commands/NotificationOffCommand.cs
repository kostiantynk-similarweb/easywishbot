﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyWishBot.Model.Extensions;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace EasyWishBot.Model.Commands
{
    public class NotificationOffCommand : CommandBase, ICommand
    {
        private readonly INotificationProvider _notificationProvider;

        public NotificationOffCommand(INotificationProvider notificationProvider, DataContext context) : base(context)
        {
            _notificationProvider = notificationProvider;
        }



        public async Task ExecuteAsync(TelegramBotClient bot, Message original, IList<string> parameteres)
        {
            var users = Context.Users.Where(s => s.ChatId == original.Chat.Id);
            foreach (var user in users)
            {
                _notificationProvider.Remove(new NotificationKey {ChatId = original.Chat.Id, User = user.GetFullName()});
            }
            

            await Task.FromResult(0);

        }
    }
}
