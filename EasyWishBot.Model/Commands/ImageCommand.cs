﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using EasyWishBot.Model.Models;
using EasyWishBot.Model.Repository;
using EasyWishBot.Model.Services;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace EasyWishBot.Model.Commands
{
    public class ImageCommand : ICommand
    {
        // https://pixabay.com/api/?key=6129997-7b0d3c8b898350c7e22d86514&q=world
        private readonly ILoggerFactory _logger;
        private readonly IImageSender _imageSender;

        public ImageCommand(ILoggerFactory logger, IImageSender imageSender)
        {
            _logger = logger;
            _imageSender = imageSender;
        }

        public async Task ExecuteAsync(TelegramBotClient bot, Message original, IList<string> parameteres)
        {
            try
            {
                var chatId = original.Chat.Id;

                if (!parameteres.Any())
                {
                    await bot.SendTextMessageAsync(chatId, "Specify a query please").ConfigureAwait(false);
                    return;
                }

                string query = string.Join(" ", parameteres);
                var imagesSent = await _imageSender.SendImagesAsync(bot, chatId, query, 5, true).ConfigureAwait(false);

                if (!imagesSent)
                {
                    await bot.SendTextMessageAsync(chatId, "No images found").ConfigureAwait(false);
                    return;
                }
            }
            catch (Exception e)
            {
                var log = _logger.CreateLogger("Errors");
                log.LogError("ImageCommand error! Parameters" + string.Join(" ", parameteres) + " |||| Error: " + e);
            }
        }
    }
}
