﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyWishBot.Model.Services;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace EasyWishBot.Model.Commands
{
    public class ImageBigCommand : ICommand
    {
        private readonly ILoggerFactory _logger;
        private readonly IImageSender _imageSender;

        public ImageBigCommand(ILoggerFactory logger, IImageSender imageSender)
        {
            _logger = logger;
            _imageSender = imageSender;
        }

        public async Task ExecuteAsync(TelegramBotClient bot, Message original, IList<string> parameteres)
        {
            try
            {
                var chatId = original.Chat.Id;

                if (!parameteres.Any())
                {
                    await bot.SendTextMessageAsync(chatId, "Specify a query please").ConfigureAwait(false);
                    return;
                }

                string query = string.Join(" ", parameteres);
                var imagesSent = await _imageSender.SendImagesAsync(bot, chatId, query, 5, false).ConfigureAwait(false);

                if (!imagesSent)
                {
                    await bot.SendTextMessageAsync(chatId, "No images found").ConfigureAwait(false);
                    return;
                }
            }
            catch (Exception e)
            {
                var log = _logger.CreateLogger("Errors");
                log.LogError("ImageBigCommand error! Parameters" + string.Join(" ", parameteres) + " |||| Error: " + e);
            }
        }
    }
}
