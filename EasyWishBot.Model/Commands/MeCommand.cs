﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EasyWishBot.Model.Extensions;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace EasyWishBot.Model.Commands
{
    public class MeCommand : CommandBase, ICommand
    {
        public MeCommand(DataContext context) : base(context)
        {
        }

        public async Task ExecuteAsync(TelegramBotClient bot, Message message, IList<string> parameteres)
        {
            var username = message.From.Username;
            var chatId = message.Chat?.Id ?? 0;
            var birthday = parameteres.ExtractFirstDateTime();
            if (parameteres?.Count > 0)
            {
                if (birthday == null)
                {
                    await bot.SendTextMessageAsync(chatId, "Birthday date format should be YYYY-MM-DD");
                    return;
                }
                var user = new User
                {
                    ChatId = chatId,
                    UserId = message.From.Id,
                    UserName = username,
                    FirstName = message.From.FirstName,
                    LastName = message.From.LastName,
                    BirthDay = birthday.Value,
                };
                Context.SetUser(user);
                await bot.SendTextMessageAsync(chatId, $"{user.GetFullName()} birthday is {user.BirthDay:yyyy-MM-dd}.");
            }
            else
            {
                var user = Context.GetUser(chatId, username);
                if (user == null)
                    await bot.SendTextMessageAsync(chatId, $"Your birthday is unknown.");
                else
                    await bot.SendTextMessageAsync(chatId, $"{user.GetFullName()} birthday is {user.BirthDay:yyyy-MM-dd}.");
            }
        }
    }
}
