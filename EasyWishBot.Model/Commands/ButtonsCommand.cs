﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InlineKeyboardButtons;
using Telegram.Bot.Types.ReplyMarkups;
using System;
using Telegram.Bot.Args;

namespace EasyWishBot.Model.Commands
{
    public class ButtonsCommand : ICommand
    {
        private readonly CommandDispatcher _dispatcher;

        public ButtonsCommand(CommandDispatcher dispatcher)
        {
            _dispatcher = dispatcher ?? throw new ArgumentNullException(nameof(dispatcher));
        }

        public async Task ExecuteAsync(TelegramBotClient bot, Message message, IList<string> parameteres)
        {
            // Keyboard
            var keyboard = new InlineKeyboardMarkup(new[]
            {
                new InlineKeyboardCallbackButton("ewme", "ewme"),
                new InlineKeyboardCallbackButton("ewwishlist", "ewwishlist"),
            });

            var chatId = message.Chat?.Id ?? 0;
            await bot.SendTextMessageAsync(chatId, "Choose buttons below:", replyMarkup: keyboard);
        }
    }
}
