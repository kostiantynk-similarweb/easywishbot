﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyWishBot.Model.Extensions;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace EasyWishBot.Model.Commands
{
    public class BirthdayListCommand : CommandBase, ICommand
    {
        public BirthdayListCommand(DataContext context) : base(context)
        {
        }

        public async Task ExecuteAsync(TelegramBotClient bot, Message message, IList<string> parameteres)
        {
            var chatId = message.Chat?.Id ?? 0;
            var users = Context.Users.Where(x => x.ChatId == chatId).OrderBy(GetDaysFromToday).ToList();
            var sb = new StringBuilder();
            sb.Append("**");
            if (parameteres.Count > 0)
            {
                var parameter = parameteres[0];
                var limit = Int32.MaxValue;
                if (parameter == "today")
                {
                    limit = 1;
                    sb.Append("Birthday list for today");
                }
                else if (parameter == "week")
                {
                    limit = 7;
                    sb.Append("Birthday list for a week");
                }
                else if (parameter == "month")
                {
                    limit = 31;
                    sb.Append("Birthday list for a month");
                }
                else if (int.TryParse(parameter, out int days) && days > 0)
                {
                    limit = days;
                    sb.Append(limit == 1 ? "Birthday list for today" : $"Birthday list for {limit} days");
                }
                else
                {
                    sb.Append("The complete birthday list");
                }
                users = users.Where(x => GetDaysFromToday(x) <= limit).ToList();
            }
            else
            {
                sb.Append("The complete birthday list");
            }
            if (users.Count == 0)
            {
                sb.Append(" is empty");
            }
            sb.AppendLine("**");
            foreach (var user in users)
            {
                sb.Append(user.BirthDay.ToString("dd MMMM"));
                sb.Append(" ");
                sb.Append(user.GetFullName());
                sb.AppendLine();
            }
            await bot.SendTextMessageAsync(chatId, sb.ToString(), ParseMode.Markdown);
        }

        private int GetDaysFromToday(User user)
        {
            var dayOfYear = DateTime.Now.DayOfYear;
            var result = user.BirthDay.DayOfYear - dayOfYear + (user.BirthDay.DayOfYear < dayOfYear ? 365 : 0);
            return result;
        }
    }
}
