﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyWishBot.Model.Extensions;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace EasyWishBot.Model.Commands
{
    public class NotificationOnCommand : CommandBase, ICommand
    {
        private readonly INotificationProvider _notificationProvider;

        public NotificationOnCommand(INotificationProvider notificationProvider, DataContext context) : base(context)
        {
            _notificationProvider = notificationProvider;
        }

        public async Task ExecuteAsync(TelegramBotClient bot, Message original, IList<string> parameteres)
        {
            var users = Context.Users.Where(s => s.ChatId == original.Chat.Id);



            foreach (var user in users)
            {
                var newDate = new DateTime(DateTime.Now.Year, user.BirthDay.Month, user.BirthDay.Day);

                //newDate = newDate.AddDays(-5);

                var diff = newDate - DateTime.Now;

                _notificationProvider.Add(new NotificationKey {ChatId = original.Chat.Id,Interval = diff.Milliseconds > 0 ? 10000 : 1 ,User = user.GetFullName(), BirthDay = user.BirthDay});

            }

            await Task.FromResult(0);
        }
    }

    public class NotificationKey
    {
        public string User { get; set; }
        public int Interval { get; set; }
        public long ChatId { get; set; }
        public DateTime BirthDay { get; set; }

        public override string ToString()
        {
            return $"{User}|{ChatId}";
        }
    }

}
