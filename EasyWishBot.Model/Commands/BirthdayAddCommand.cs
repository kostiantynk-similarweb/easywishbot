﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyWishBot.Model.Extensions;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace EasyWishBot.Model.Commands
{
    public class BirthdayAddCommand : CommandBase, ICommand
    {
        private readonly ILoggerFactory _logger;

        public BirthdayAddCommand(DataContext context, ILoggerFactory logger) : base(context)
        {
            _logger = logger;
        }

        public async Task ExecuteAsync(TelegramBotClient bot, Message message, IList<string> parameteres)
        {
            try
            {
                var chatId = message.Chat?.Id ?? 0;
                var targetuser = message.ExtractFirstEntity(MessageEntityType.Mention) ?? message.ExtractFirstEntity(MessageEntityType.TextMention);
                var birthday = parameteres.ExtractFirstDateTime();
                if (targetuser == null || birthday == null)
                {
                    await bot.SendTextMessageAsync(chatId, "Specify user name and date in format YYYY-MM-DD");
                    return;
                }
                var user = new User
                {
                    ChatId = chatId,
                    UserName = targetuser.Item2.StartsWith("@") ? targetuser.Item2.Substring(1) : targetuser.Item2,
                    UserId = targetuser.Item1.User?.Id ?? 0,
                    FirstName = targetuser.Item1.User?.FirstName,
                    LastName = targetuser.Item1.User?.LastName,
                    BirthDay = birthday.Value
                };
                Context.SetUser(user);
                await bot.SendTextMessageAsync(chatId, $"{user.GetFullName()} birthday is {user.BirthDay:yyyy-MM-dd}.");
            }
            catch (Exception e)
            {
                var log = _logger.CreateLogger("Errors");
                log.LogError("BirthdayAddCommand error! Parameters" + string.Join(" ", parameteres) + " |||| Error: " + e);
            }
        }
    }
}
