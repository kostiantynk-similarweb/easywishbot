﻿namespace EasyWishBot.Model.Commands
{
    public abstract class CommandBase
    {
        protected CommandBase(DataContext context)
        {
            Context = context;
        }

        public DataContext Context { get; }
    }
}
