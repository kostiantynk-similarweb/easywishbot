﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyWishBot.Model.Extensions;
using EasyWishBot.Model.Models;
using EasyWishBot.Model.Repository;
using Microsoft.Extensions.Logging;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace EasyWishBot.Model.Commands
{
    public class WishCommand : ICommand
    {
        private const string SPECIFY_WISH_RESPONSE = "Please specify your wish";
        private const string SUCCESS_WISH = "Your wish successfully added!";

        private readonly IWishRepository _repository;
        private readonly ILoggerFactory _logger;

        public WishCommand(IWishRepository repository, ILoggerFactory logger)
        {
            _repository = repository;
            _logger = logger;
        }

        public async Task ExecuteAsync(TelegramBotClient bot, Message original, IList<string> parameteres)
        {
            try
            {
                string author = original.From.Username;
                var chatId = original.Chat.Id;

                if (!parameteres.Any())
                {
                    await bot.SendTextMessageAsync(chatId, SPECIFY_WISH_RESPONSE).ConfigureAwait(false);
                    return;
                }

                var toUser = original.ExtractFirstEntity(MessageEntityType.Mention) ?? original.ExtractFirstEntity(MessageEntityType.TextMention);

                if (toUser != null)
                {
                    if (parameteres.Count <= 1)
                    {
                        await bot.SendTextMessageAsync(chatId, SPECIFY_WISH_RESPONSE);
                        return;
                    }

                    string message = string.Join(" ", parameteres.Skip(1));

                    var userName = toUser.Item2.StartsWith("@") ? toUser.Item2.Substring(1) : toUser.Item2;
                    Wish newWish = new Wish { Id = Guid.NewGuid(), Author = author, Message = message, UserName = userName };
                    _repository.Create(newWish);

                    await bot.SendTextMessageAsync(chatId, SUCCESS_WISH).ConfigureAwait(false);
                }
                else
                {
                    // author wishes to himself
                    string message = string.Join(" ", parameteres);

                    Wish newWish = new Wish { Author = author, Id = Guid.NewGuid(), Message = message, UserName = author };
                    _repository.Create(newWish);

                    await bot.SendTextMessageAsync(chatId, SUCCESS_WISH).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                var log = _logger.CreateLogger("Errors");
                log.LogError("WishCommand error! Parameters" + string.Join(" ", parameteres) + " |||| Error: " + e);
            }
        }
    }
}
