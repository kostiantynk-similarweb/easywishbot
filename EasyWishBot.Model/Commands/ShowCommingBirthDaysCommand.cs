﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyWishBot.Model.Extensions;
using EasyWishBot.Model.Services;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace EasyWishBot.Model.Commands
{
   public class ShowCommingBirthDaysCommand: CommandBase, ICommand
    {
        private readonly IImageSender _imageSender;

        public ShowCommingBirthDaysCommand(DataContext context, IImageSender imageSender) : base(context)
        {
            _imageSender = imageSender;
        }

        public async Task ExecuteAsync(TelegramBotClient bot, Message original, IList<string> parameteres)
        {

            var usersWithUpcomingBirthDays = Context.Users.Where(s => GetUpcomingBirthDay(s));


            foreach (var user in usersWithUpcomingBirthDays)
            {
               await bot.SendTextMessageAsync(user.ChatId, $"{user.GetFullName()}'s birthday is comming in 7 days");
            }


            var birthdayToday = Context.Users.Where(s => GetTodayBirthDay(s));

            foreach (var user in birthdayToday)
            {
               await bot.SendTextMessageAsync(user.ChatId, $" Happy birthday {user.GetFullName()}");

              await  _imageSender.SendImagesAsync(bot, user.ChatId, "birthday cake", 5, false);
            }

        }

        private bool GetUpcomingBirthDay(User user)
        {
            var currentYearBirthDay = new DateTime(DateTime.Now.Year, user.BirthDay.Month, user.BirthDay.Day);

            var diff = currentYearBirthDay - DateTime.Now;
            return diff.Days <= 7 && diff.Days != 0;
        }

        private bool GetTodayBirthDay(User user)
        {
            var currentYearBirthDay = new DateTime(DateTime.Now.Year, user.BirthDay.Month, user.BirthDay.Day);

            var diff = currentYearBirthDay - DateTime.Now;
            return diff.Days == 0;
        }
    }
}
