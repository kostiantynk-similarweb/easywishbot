﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace EasyWishBot.Model.Commands
{
    public class StartCommand : ICommand
    {
        public async Task ExecuteAsync(TelegramBotClient bot, Message message, IList<string> parameteres)
        {
            var chatId = message.Chat?.Id ?? 0;
            await bot.SendTextMessageAsync(chatId,
@"/ewme                                                           - my birthday
/ewme YYYY-MM-DD                                - set your birthday
/ewadd @username YYYY-MM-DD     - add/edit user birthday
/ewlist                                                           - show birthdays list

/ewnotifyon                                                - turn on notification
/ewnotifyoff                                                - turn off notification

/ewwish  <text>                                         - add your present 
/ewwish @username <text>                 - add user present 
/ewwishlist                                                  - show wish list

/ewimage  /ewpicture <topic>             - show image 
/ewshowcomming                                   - show upcoming events");
        }
    }
}
