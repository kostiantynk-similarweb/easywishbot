﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace EasyWishBot.Model.Commands
{
    public class CommandDispatcher
    {
        private readonly Dictionary<string, ICommand> _commands = new Dictionary<string, ICommand>();

        public CommandDispatcher(DataContext context, IDictionary<string, ICommand> commands)
        {
            if (commands == null)
            {
                throw new ArgumentNullException(nameof(commands));
            }

            foreach (var command in commands)
            {
                _commands.Add(command.Key, command.Value);
            }
        }

        public void RegisterCommand(string key, ICommand command)
        {
            _commands[key] = command;
        }

        public async Task Dispatch(TelegramBotClient bot, string key, IList<string> parameters, Message message)
        {
            var chatId = message?.Chat?.Id;
            if (_commands.ContainsKey(key))
            {
                await _commands[key].ExecuteAsync(bot, message, parameters);
            }
            // answer only to recognized commands
            /*else
            {
                await bot.SendTextMessageAsync(chatId, $"Command {key} is not supported.");
            }*/
        }
    }
}
