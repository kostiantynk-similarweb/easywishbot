﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EasyWishBot.Model.Extensions;
using EasyWishBot.Model.Models;
using EasyWishBot.Model.Repository;
using EasyWishBot.Model.Services;
using Microsoft.Extensions.Logging;
using Remotion.Linq.Clauses;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace EasyWishBot.Model.Commands
{
    public class WishListCommand : ICommand
    {
        private readonly IWishRepository _repository;
        private readonly ILoggerFactory _logger;
        private readonly IImageSender _imageSender;

        public WishListCommand(IWishRepository repository, ILoggerFactory logger, IImageSender imageSender)
        {
            _repository = repository;
            _logger = logger;
            _imageSender = imageSender;
        }

        public async Task ExecuteAsync(TelegramBotClient bot, Message original, IList<string> parameteres)
        {
            try
            {
                string author = original.From.Username;
                var chatId = original.Chat.Id;

                var users = original.ExtractEntities(MessageEntityType.Mention)
                    .Concat(original.ExtractEntities(MessageEntityType.TextMention))
                    .Select(x=> x.Item2.StartsWith("@") ? x.Item2.Substring(1) : x.Item2 )
                    .ToList();

                var wishes = users == null || !users.Any()
                    ? _repository.GetAll()
                    : _repository.GetAll().Where(x => users.Contains(x.UserName)).ToList();

                string message = GetWishesMessage(wishes);
                await bot.SendTextMessageAsync(chatId, message).ConfigureAwait(false);

                if (users != null && users.Count == 1)
                {
                    var shortWishes = wishes.Where(x => x.Message.Split(' ').Length <= 2).ToList();

                    List<Task> tasks = new List<Task>();                    
                    foreach (var w in shortWishes)
                    {
                        tasks.Add(_imageSender.SendImagesAsync(bot, chatId, w.Message, 5, true));
                    }

                    try
                    {
                        await Task.WhenAll(tasks).ConfigureAwait(false);
                    }
                    catch (Exception e)
                    {
                        // ignored
                    }
                    
                }
            }
            catch (Exception e)
            {
                var log = _logger.CreateLogger("Errors");
                log.LogError("WishListCommand error! Parameters" + string.Join(" ", parameteres) + " |||| Error: " + e);
            }
        }

        private string GetWishesMessage(IList<Wish> wishes)
        {
            StringBuilder message = new StringBuilder();
            wishes.GroupBy(x => x.UserName)
                .Select(x => message.AppendLine($"{x.Key} -> {string.Join(", ", x.Select(y => y.Message))} " + Environment.NewLine)).ToArray();

            return message.ToString();
        }
    }
}
