﻿using System.Collections.Generic;
using EasyWishBot.Api.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using EasyWishBot.Model;
using EasyWishBot.Model.Commands;
using EasyWishBot.Model.Repository;
using EasyWishBot.Model.Services;
using Telegram.Bot;

namespace EasyWishBot.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc(options =>
            {
                options.Filters.Add(typeof(ExceptionFilter));
            });

            services.AddSingleton<ILoggerFactory, LoggerFactory>();
            services.AddLogging();

            services.AddSingleton<IConfiguration>(Configuration);
            services.AddDbContext<DataContext>(opt => opt.UseInMemoryDatabase());

            // Add telegram bot
            services.AddSingleton(new TelegramBotClient(Configuration["TelegramBot:Token"]));

            // Add providers
            services.AddSingleton<IWishRepository, WishRepository>();
            services.AddSingleton<IImageGetter, ImageGetter>();
            services.AddSingleton<IImageSender, ImageSender>();

            var notificationProvider = new NotificationProvider(Configuration["TelegramBot:Token"]);
            services.AddSingleton<INotificationProvider>(notificationProvider);

            services.AddSingleton<IBirthDayCheker, BirthDayCheker>();

            // Add commands
            services.AddSingleton<MeCommand>();
            services.AddSingleton<BirthdayAddCommand>();
            services.AddSingleton<BirthdayListCommand>();
            services.AddSingleton<StartCommand>();
            services.AddSingleton<WishCommand>();
            services.AddSingleton<WishListCommand>();
            services.AddSingleton<NotificationOnCommand>();
            services.AddSingleton<NotificationOffCommand>();
            services.AddSingleton<ImageCommand>();
            services.AddSingleton<ImageBigCommand>();
            services.AddSingleton<ShowCommingBirthDaysCommand>();

            services.AddSingleton(x => new CommandDispatcher(x.GetService<DataContext>(), new Dictionary<string, ICommand>
            {
                ["ewadd"] = x.GetService<BirthdayAddCommand>(),
                ["ewlist"] = x.GetService<BirthdayListCommand>(),
                ["ewme"] = x.GetService<MeCommand>(),
                ["ewnotifyon"] = x.GetService<NotificationOnCommand>(),
                ["ewnotifyoff"] = x.GetService<NotificationOffCommand>(),
                ["start"] = x.GetService<StartCommand>(),
                ["ewwish"] = x.GetService<WishCommand>(),
                ["ewwishlist"] = x.GetService<WishListCommand>(),
                ["ewimage"] = x.GetService<ImageCommand>(),
                ["ewpicture"] = x.GetService<ImageBigCommand>(),
                ["ewshowcomming"] = x.GetService<ShowCommingBirthDaysCommand>()
            }));

            services.AddSingleton<ButtonsCommand>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            var dispatcher = app.ApplicationServices.GetService<CommandDispatcher>();
            dispatcher.RegisterCommand("ewhelp", new ButtonsCommand(dispatcher));

            //var birthDayChecker = app.ApplicationServices.GetService<BirthDayCheker>();
            //birthDayChecker.Init();

            app.UseMvc();
        }
    }
}
