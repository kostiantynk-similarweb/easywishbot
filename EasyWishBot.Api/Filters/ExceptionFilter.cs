﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace EasyWishBot.Api.Filters
{
    public class ExceptionFilter : IExceptionFilter
    {
        private readonly ILoggerFactory _logger;

        public ExceptionFilter(ILoggerFactory logger)
        {
            _logger = logger;
        }

        public void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            var log = _logger.CreateLogger("Errors");
            log.LogError($"Error occured! ActionDescriptor: {context.ActionDescriptor.DisplayName} ||| Exception: {exception}");
        }
    }
}
