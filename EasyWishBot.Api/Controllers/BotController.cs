﻿using System;
using System.Threading.Tasks;
using System.Linq;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

using EasyWishBot.Model.Commands;
using EasyWishBot.Model.Extensions;
using Telegram.Bot;
using Microsoft.Extensions.Logging;

namespace EasyWishBot.Api.Controllers
{
    [Route("api/[controller]")]
    public class BotController : Controller
    {
        private readonly IConfiguration _configuration;

        private readonly CommandDispatcher _dispatcher;

        private readonly TelegramBotClient _bot;

        private readonly ILoggerFactory _logger;

        public BotController(IConfiguration configuration, CommandDispatcher dispatcher, TelegramBotClient bot, ILoggerFactory logger)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
            _dispatcher = dispatcher ?? throw new ArgumentNullException(nameof(dispatcher));
            _bot = bot ?? throw new ArgumentNullException(nameof(bot));
            _logger = logger;
        }

        [HttpGet]
        [HttpPost]
        public async Task<string> Post([FromBody] Update update)
        {
            try
            {
                var tag = _configuration["TelegramBot:Tag"];
                var message = update.Message;
                if (message?.Type == MessageType.TextMessage && message.Chat != null)
                {
                    var queryParams = message.Text
                        .Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                        .Where(x => !String.Equals(x, tag, StringComparison.CurrentCultureIgnoreCase)).ToList();
                    var command = message.ExtractFirstEntity(MessageEntityType.BotCommand).Item2.Substring(1);
                    var index = command.IndexOf('@');
                    if (index > 0)
                        command = command.Substring(0, index);
                    var parameters = queryParams.Where(x => !x.StartsWith("/")).ToList();
                    await _dispatcher.Dispatch(_bot, command, parameters, message);
                }
            }
            catch (Exception ex)
            {
                var log = _logger.CreateLogger("Error");
                log.LogError(ex.ToString());
            }
            return "OK";
        }
    }
}
