﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyWishBot.Model.Commands;
using EasyWishBot.Model.Repository;
using EasyWishBot.Model.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace EasyWishBot.Api.Controllers
{
    [Route("api/test")]
    public class TestController : Controller
    {
        private readonly IWishRepository _repository;
        private readonly ILoggerFactory _logger;
        private readonly IImageGetter _imageGetter;

        public TestController(IWishRepository repository, ILoggerFactory logger, IImageGetter getter)
        {
            _repository = repository;
            _logger = logger;
            _imageGetter = getter;
        }

        [HttpGet]
        public async Task<string> Test()
        {
            var t = await _imageGetter.GetImages("boobs", 5, false);
            var log = _logger.CreateLogger("Error");
            log.LogInformation("hello");
            return "Ok";
        }
    }
}
